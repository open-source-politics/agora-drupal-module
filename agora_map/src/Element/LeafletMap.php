<?php

namespace Drupal\agora_map\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a form element for a map element.
 *
 * @FormElement("address_leaflet_map")
 */
class LeafletMap extends WebformCompositeBase {

  const STATE_GEO_SUCCESS = 'success';
  const STATE_GEO_FAILURE = 'failure';

	/**
	 * {@inheritdoc}
	 */
	public static function getCompositeElements(array $element) {
		$elements = [];
		$elements['address'] = [
			'#type' => 'textfield',
			'#title' => t('Address'),
		];
		$elements['city'] = [
			'#type' => 'textfield',
			'#title' => t('City/Town'),
		];
		$elements['postal_code'] = [
			'#type' => 'textfield',
			'#title' => t('Zip/Postal Code'),
		];
		$elements['country'] = [
			'#type' => 'select',
			'#title' => t('Country'),
			'#options' => 'country_names',
		];
		return $elements;
	}

}
