<?php
namespace Drupal\agora_map\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Map block.
 *
 * @Block(
 *	 id = "agora_map_block",
 *	 admin_label = @Translation("Map block"),
 * )
 */
class AgoraMapBlock extends BlockBase {
	// Override BlockPluginInterface methods here.

	/**
	 * {@inheritdoc}
	 */
	public function build() {
		$maps = leaflet_leaflet_map_info();
		return leaflet_render_map(array(
			'label' => 'OSP Agora map',
			'description' => t('Carte listant les lieux'),
			'settings' => array(
				'dragging' => TRUE,
				'touchZoom' => TRUE,
				'scrollWheelZoom' => TRUE,
				'doubleClickZoom' => TRUE,
				'zoomControl' => TRUE,
				'attributionControl' => TRUE,
				'trackResize' => TRUE,
				'fadeAnimation' => TRUE,
				'zoomAnimation' => TRUE,
				'closePopupOnClick' => TRUE,
				'center' => array('lat' => 48.850445, 'lng' => 2.373448),
				'zoom' => 12
			),
			'layers' => array(
				'earth' => array(
					'urlTemplate' => 'https://a.tiles.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
					'options' => array(
						'attribution' => 'OSP Agora map',
						'maxZoom' =>	18,
						'id' => 'arcinnovation/cilb10ytg00ecegm0lxz8v09l',
						'accessToken' => 'pk.eyJ1IjoiYXJjaW5ub3ZhdGlvbiIsImEiOiJjaWxhdGR0dW8wMDI2d3FtMnB1cXVteDNrIn0.p7whVJKJPEdmxnU9acTlSg',
					)
				),
			),
		), array( // features
			array(
				'type' => 'point',
				'lat' => 48.8900491,
				'lon' => 2.3706808,
				//'icon' => array(
				//	'iconUrl' => 'sites/default/files/mymarker.png'
				//),
				//'popup' => l($node->title, 'node/' . $node->nid),
				'popup' => '104 - 5 rue Curial',
				//'leaflet_id' => 'some unique ID'
			)
		), '600px');
	}
}

