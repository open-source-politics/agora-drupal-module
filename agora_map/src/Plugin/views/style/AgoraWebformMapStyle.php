<?php
namespace Drupal\agora_map\Plugin\views\style;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

use Drupal\Component\Utility\NestedArray;
use Drupal\agora_map\Element\LeafletMap as LeafletMapElement;

/**
 * A Views style that renders a Leaflet Agora Map.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *	 id = "agora_webform_map",
 *	 title = @Translation("Agora Webform Map"),
 *	 help = @Translation("Display points on an Agora Leaflet Map"),
 * )
 */
class AgoraWebformMapStyle extends StylePluginBase {
	protected $usesOptions = true;
	protected $usesGrouping = false;
	protected $usesFields = false;

  protected $libgeophp = false;
  protected $shapes = false;

  public function geophp() {
    if (!$this->libgeophp) {
      $this->libgeophp = \Drupal::service('geofield.geophp');
    }
    return $this->libgeophp;
  }

  public function isDebug(){
    return array_key_exists('debug',$this->options['properties'])
      && $this->options['properties']['debug'] ;
  }

	/**
	 * Set default options
	 */
	protected function defineOptions() {
		$options = parent::defineOptions();
		$options['properties'] = array('default' => [
      'height' => 800,
      'form_key' => 'address',
      'debug' => false,
    ]);
		return $options;
	}

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['properties'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Custom properties'),
      '#default_value' => $this->options['properties'] ,
    ];

  }

  protected function loadShapes( $file, $key ) {
    $raw = file_get_contents( drupal_get_path('module', 'agora_map') . '/' . $file );
    if ( $raw ) {
      $json = json_decode($raw, true);

      $this->shapes = array();
      if( isset($json['type']) &&  $json['type'] == 'FeatureCollection' ) {
        foreach ($json['features'] as $val) {

          if( isset($key) && array_key_exists($key, $val['properties']) ) {
            $id = $val['properties'][$key];
          } else {
            $id = hash('md5',serialize($val['properties']));
          }

          $this->shapes[$id] = array(
            'id' => $id,
            'properties' => array(
              'count' => 0,
            ) + $val['properties'],
            'geojson' => $val['geometry'],
            'geophp' => $this->geophp()->load(json_encode($val['geometry']),'geojson'),
          );
        }
      }
    }

  }

  public function preRender($result) {

    if (!empty($this->view->rowPlugin)) {
      $this->view->rowPlugin->preRender($result);
    }

    if ( isset($this->options['properties']['map_cluster']) && $this->options['properties']['map_cluster']['type'] == 'polygon' ) {
      $this->loadShapes($this->options['properties']['map_cluster']['file'],$this->options['properties']['map_cluster']['key']);
    }

  }

	/**
	* Renders the View.
	*/
	public function render() {
		$features = array();

    //dcp('Agora Webform Map Style render view');


    if ($this->options['properties']['map_cluster']['type'] == 'markercluster'){
      // add the marker cluster
  		$features[] = array(
  			'type' => 'markercluster',
  		);
    }


    foreach ($this->view->result as $id => $result) {

      if ( get_class($result->_entity) !== 'Drupal\webform\Entity\WebformSubmission' ) {
        if ($this->view->preview) {
          return '<pre>[Views ERROR][AgoraWebformMapStyle] Results must be Drupal\webform\Entity\WebformSubmission</pre>';
        } else {
          return '';
        }
      }

      $data = $result->_entity->getData($this->options['properties']['form_key']);

      if ( $data['geo_status'] == LeafletMapElement::STATE_GEO_SUCCESS ) {
        if ($this->options['properties']['map_cluster']['type'] == 'polygon'){
          $features[] = array(
            'type' => 'point',
            'lat' => $data['latitude'],
            'lon' => $data['longitude'],
          );
        }

        if ( $this->shapes && array_key_exists('cluster_id',$data) ) {
          $this->shapes[$data['cluster_id']]['properties']['count']++ ;
        }
      }

    }

    if ( $this->shapes ) {
      foreach ($this->shapes as $key => $shape) {
        // $center = array('lat' => $shape['geophp']->getCentroid()->getX(), 'lng' => $shape['geophp']->getCentroid()->getY());
        // remove true if you don't to display zone without result
        if ( true || $shape['properties']['count'] ) {
          $geojson_features[] = array(
            'type' => 'Feature',
            'geometry' => $shape['geojson'], // JSON Object is needed
            'debug' => $this->isDebug(),
            'properties' => $shape['properties'],
          );
        }
      }
      $geojson_shapes = array(
        'type' => 'FeatureCollection',
        'features' => $geojson_features,
      );
      $features[] = array(
        'type' => 'json-choropleth',
        'json' => $geojson_shapes, // JSON Object is needed
        'properties' => array(
          'style' => array(
            'color' => '#fff', // border color
            'weight' => 2,
            'fillOpacity' => 0.8,
          ),
          'leaflet_id' => 'agora-webform-map-' + $this->options['properties']['map_cluster']['key']
        )
      );
      $geophp_shapes = $this->geophp()->load(json_encode($geojson_shapes),'geojson');
      $center = array('lat' => $geophp_shapes->getCentroid()->getX(), 'lng' => $geophp_shapes->getCentroid()->getY());
    }


    // return '<pre>RENDER VIEW</pre>';
    // //dcp($center);
    // //dcp($features);
    $render = \Drupal::service('leaflet.service')->leafletRenderMap(array(
      'label' => 'OSP Agora',
      'settings' => array(
        'dragging' => TRUE,
        'touchZoom' => TRUE,
        'scrollWheelZoom' => TRUE,
        'doubleClickZoom' => TRUE,
        'zoomControl' => TRUE,
        'attributionControl' => TRUE,
        'trackResize' => TRUE,
        'fadeAnimation' => TRUE,
        'zoomAnimation' => TRUE,
        'minZoom' => 11,
        'closePopupOnClick' => TRUE,
        'center' => isset($center) ? $center : array('lat' => 48.850445, 'lng' => 2.373448),
        'zoom' => 14
      ),
      'layers' => array(
        'earth' => array(
          // 'urlTemplate' => 'https://a.tiles.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
          // 'urlTemplate' => 'https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
          'urlTemplate' => '//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
          // 'urlTemplate' => 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
          'options' => array(
            'attribution' => 'OSP Agora',
            'maxZoom' =>	18,
            'CStyle' => 'digits',
            'animate' => true,
            // 'id' => 'opensourcepolitics/ciwjaci0200362pqpii95xz65',
            // 'accessToken' => 'pk.eyJ1Ijoib3BlbnNvdXJjZXBvbGl0aWNzIiwiYSI6ImNpd2phYzI4NjAwMGEyb21zYjFkajZ0NTQifQ.c0s258Vrsz8RAyNzOaPx3g',
          )
        ),
      ),
    ), $features , $this->options['properties']['height'] . 'px');

    $render['#attached'] = NestedArray::mergeDeep(array(
      'library' => array(
        'agora_map/leaflet.choropleth',
        'agora_map/leaflet.markercluster',
        'agora_map/leaflet.fullscreen',
        'agora_map/leaflet.providers',
        // 'agora_map/leaflet.geoportal',
      )
    ), $render['#attached']);

    // if ($this->view->preview) {
    //   $data = array();
    //   $data += $this->options['properties'];
    //   // $data['render'] = $render ;
    //   return '<pre>'. print_r($data,1) .'</pre>';
    // }

    return $render;

	}
}
