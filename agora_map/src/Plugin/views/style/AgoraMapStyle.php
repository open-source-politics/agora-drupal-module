<?php
namespace Drupal\agora_map\Plugin\views\style;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Drupal\Core\Serialization\Yaml;
use Drupal\Component\Utility\NestedArray;

/**
 * A Views style that renders a Leaflet Agora Map.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *	 id = "agora_map",
 *   theme = "agora-map",
 *	 title = @Translation("Agora Map"),
 *	 help = @Translation("Display points on an Agora Leaflet Map"),
 * )
 */
class AgoraMapStyle extends StylePluginBase {
	protected $usesOptions = true;
	protected $usesGrouping = false;
	protected $usesFields = true;

	/**
	 * Set default options
	 */
	protected function defineOptions() {
		$options = parent::defineOptions();

    $options['agora'] = [
      'default' => Yaml::encode([
        'height' => '800px',
        'maxZoom' => 18,
        'animate' => true, // CSS animation for markers
        'CStyle' => 'digits', // reference to corporate ID guide style - other possible values : C11, C8, C19 or digits
        'view_mode' => 'map_tooltip',
        'sidebar' => false,
        'attribution' => 'OSP Agora map',
        'id' => 'opensourcepolitics/ciwjaci0200362pqpii95xz65',
        'accessToken' => 'pk.eyJ1Ijoib3BlbnNvdXJjZXBvbGl0aWNzIiwiYSI6ImNpd2phYzI4NjAwMGEyb21zYjFkajZ0NTQifQ.c0s258Vrsz8RAyNzOaPx3g',
      ])
    ];

		return $options;
	}

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    // $exposed = array(
    //   'height' => $this->options['agora']['height'],
    //   'maxZoom' => $this->options['agora']['maxZoom'],
    //   'animate' => $this->options['agora']['animate'],
    //   'attribution' => $this->options['agora']['attribution'],
    // );

    $form['agora'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#default_value' => $this->options['agora'],
      '#description' => $this->t('View options in YAML format'),
    ];
  }

  public function preRender($result) {
    dcp('AgoraMapStyle preRender view');
    dcp($result);

    if (!empty($this->view->rowPlugin)) {
      $this->view->rowPlugin->preRender($result);
    }
  }

	/**
	* Renders the View.
	*/
	public function render() {
    dcp('AgoraMapStyle render view');

    $points = array();
    $agora_options = Yaml::decode($this->options['agora']);
    if( !array_key_exists('view_mode',$agora_options) ) {
      $agora_options['view_mode'] = 'map_tooltip';
    }

    if( array_key_exists('sidebar',$agora_options) && $agora_options['sidebar'] ) {
      $points[] = array(
  			'type' => 'sidebar',
  		);
    }

		// add the marker cluster
		$points[] = array(
			'type' => 'markercluster',
		);

		// first, get provided fields names
		$fields = array_keys($this->view->field);

    // get Height
    if((count($this->view->args) > 0) && (intval($this->view->args[0]))) {
      $height = $this->view->args[0] ;
    } else {
      $height = $agora_options['height'] ;
    }

		// parse view results
		foreach ($this->view->result as $id => $result) {
      $node = \Drupal\node\Entity\Node::load($result->nid);

			// first field should be geocoding field
			if( $node->{$fields[0]}->lat != null ) {
        //following field is the popup text
        $builder = \Drupal::entityTypeManager()->getViewBuilder('node')->view($node, $agora_options['view_mode']);
				$popup = render($builder);
				$points[] = array(
					'type' => 'point',
					'lat' => $node->{$fields[0]}->lat,
					'lon' => $node->{$fields[0]}->lon,
					'popup' => $popup,
          'leaflet_id' => $result->nid,
				);
			}
		}


    // render the map with plotted points
		$render = \Drupal::service('leaflet.service')->leafletRenderMap(array(
  			'label' => 'arc de l\'innovation',
  			'description' => t('Carte listant les lieux'),
  			'settings' => array(
  				'dragging' => TRUE,
  				'touchZoom' => TRUE,
  				'scrollWheelZoom' => FALSE,
  				'doubleClickZoom' => TRUE,
  				'zoomControl' => TRUE,
  				'attributionControl' => TRUE,
  				'trackResize' => TRUE,
  				'fadeAnimation' => TRUE,
  				'zoomAnimation' => TRUE,
  				'closePopupOnClick' => TRUE,
  				'center' => array('lat' => 48.850445, 'lng' => 2.373448),
          'fullscreenControl' => FALSE,
          // // OR
          // 'fullscreenControl' => array(
          //     'pseudoFullscreen' => TRUE // if true, fullscreen to page width and height
          // )
  			),
  			'layers' => array(
  				'earth' => array(
  					'urlTemplate' => 'https://a.tiles.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
  					'options' => $agora_options
  				),
  			),
      // ), $points, $height.'px');
    ), $points, $height);

    $render['#attached'] = NestedArray::mergeDeep(array(
      'library' => array(
        'agora_map/leaflet.choropleth',
        'agora_map/leaflet.markercluster',
        'agora_map/leaflet.fullscreen',
        'agora_map/leaflet.providers',
        'agora_map/leaflet.sidebar-v2',
        // 'agora_map/leaflet.geoportal',
      )
    ), $render['#attached']);

    // if ($this->view->preview) {
    //   $data = array();
    //   $data += $this->options['properties'];
    //   // $data['render'] = $render ;
    //   return '<pre>'. print_r($data,1) .'</pre>';
    // }

    return $render;




  }
}
