<?php

namespace Drupal\agora_map\Plugin\WebformElement;
use Drupal\agora_map\Element\LeafletMap as LeafletMapElement;

use Drupal\webform\Entity\WebformSubmission;
use Drupal\webform\WebformElementBase;
use Drupal\webform\Plugin\WebformElement\WebformAddress;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\Utility\WebformOptionsHelper;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Session\AccountInterface;
use Drupal\Component\Render\HtmlEscapedText;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormState;

use Drupal\Core\Render\Element as RenderElement;

/**
 * Provides a 'leaflet_map' element.
 *
 * @WebformElement(
 *	 id = "address_leaflet_map",
 *	 label = @Translation("Map"),
 *	 category = @Translation("Composite elements"),
 *	 multiline = TRUE,
 *	 composite = TRUE,
 *	 states_wrapper = TRUE,
 * )
 */
class LeafletMap extends WebformCompositeBase {

  protected $libgeophp = false;
  protected $shapes = false;

  public function geophp() {
    if (!$this->libgeophp) {
      $this->libgeophp = \Drupal::service('geofield.geophp');
    }
    return $this->libgeophp;
  }

	public function getCompositeElements() {
		return LeafletMapElement::getCompositeElements();
	}

	/**
	* {@inheritdoc}
	*/
	public function getInitializedCompositeElement(array $element, $composite_key = NULL) {
		$form_state = new FormState();
		$form_completed = [];

		return LeafletMapElement::processWebformComposite($element, $form_state, $form_completed);
	}

  public function getDefaultProperties() {
    $properties = [
      'preview_message_content' => '',
      'preview_message_wrapper_attributes' => [],
      'not_found_message_content' => '',
      'not_found_message_wrapper_attributes' => [],
      'format_items' => $this->getItemsDefaultFormat(),
      'format_preview' => $this->getItemsDefaultFormat(),
      'format_email' => 'text',
    ] + parent::getDefaultProperties();

    return $properties;
  }

  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['display']['format_preview'] = [
      '#type' => 'select',
      '#title' => $this->t('Preview format'),
      '#description' => $this->t('Select how the component is displayed in preview'),
      '#options' => $this->getItemFormats(),
    ];
    $form['display']['format_email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email format'),
      '#description' => $this->t('Select how the component is displayed in email'),
      '#options' => $this->getItemFormats(),
    ];

    $form['preview_message'] = [
      '#type' => 'details',
      '#title' => $this->t('Preview message'),
      '#access' => TRUE,
      '#weight' => -20,
    ];
    $form['preview_message']['preview_message_content'] = [
      '#type' => 'webform_html_editor',
      '#access' => TRUE,
      '#title' => $this->t('Content'),
      '#description' => $this->t('The preview message.'),
    ];
    $form['preview_message']['preview_message_wrapper_attributes'] = [
      '#type' => 'webform_element_attributes',
      '#title' => $this->t('Wrapper'),
      '#access' => TRUE,
      '#class__description' => $this->t("Apply classes to the element's wrapper around both the field and its label. Select 'custom...' to enter custom classes."),
      '#style__description' => $this->t("Apply custom styles to the element's wrapper around both the field and its label."),
      '#attributes__description' => $this->t("Enter additional attributes to be added the element's wrapper."),
      '#classes' => $this->configFactory->get('webform.settings')->get('elements.wrapper_classes'),
    ];

    $form['not_found_message'] = [
      '#type' => 'details',
      '#title' => $this->t('Not found message'),
      '#access' => TRUE,
      '#weight' => -20,
    ];
    $form['not_found_message']['not_found_message_content'] = [
      '#type' => 'webform_html_editor',
      '#title' => $this->t('Content'),
      '#access' => TRUE,
      '#description' => $this->t('The message displayed if geocoding fails.'),
    ];
    $form['not_found_message']['not_found_message_wrapper_attributes'] = [
      '#type' => 'webform_element_attributes',
      '#title' => $this->t('Wrapper'),
      '#access' => TRUE,
      '#class__description' => $this->t("Apply classes to the element's wrapper around both the field and its label. Select 'custom...' to enter custom classes."),
      '#style__description' => $this->t("Apply custom styles to the element's wrapper around both the field and its label."),
      '#attributes__description' => $this->t("Enter additional attributes to be added the element's wrapper."),
      '#classes' => $this->configFactory->get('webform.settings')->get('elements.wrapper_classes'),
    ];


    return $form;
  }

  protected function mergeDefaultValue(array &$element, array &$value) {
    if (array_key_exists('#default_value',$element)) {
      foreach ($element['#default_value'] as $k => $v) {
        if(empty($value[$k])) {
          $value[$k] = $v;
        }
      }
    }
  }

  // public function finalize(array &$element, WebformSubmissionInterface $webform_submission) {
  //   // Prepare multiple element.
  //   parent::finalize($element,$webform_submission);
  //   //dcp('finalize mod');
  //   //dcp($webform_submission->toArray());
  // }

  public function preSave(array &$element, WebformSubmissionInterface $webform_submission) {
    if ( $webform_submission->isCompleted() ) {
      $data = $webform_submission->getData();
      $this->mergeDefaultValue($element, $data[$element['#webform_key']]);
      $this->processGeolocalisation($element,$data[$element['#webform_key']]);
      $webform_submission->setData($data);

    }
  }

  protected function loadShapes( $file, $key ) {
    $raw = file_get_contents( drupal_get_path('module', 'agora_map') . '/' . $file );
    if ( $raw ) {
      $json = json_decode($raw, true);

      $this->shapes = array();
      if( isset($json['type']) &&  $json['type'] == 'FeatureCollection' ) {
        foreach ($json['features'] as $val) {

          if( isset($key) && array_key_exists($key, $val['properties']) ) {
            $id = $val['properties'][$key];
          } else {
            $id = hash('md5',serialize($val['properties']));
          }

          $this->shapes[$id] = array(
            'id' => $id,
            'properties' => $val['properties'],
            'geojson' => $val['geometry'],
            'geophp' => $this->geophp()->load(json_encode($val['geometry']),'geojson'),
          );
        }
      }
    }

  }

  protected function safeString($s) {
    if( is_array($s) ) {
      return implode(' ', $s);
    } else {
      return strval($s);
    }
  }

  protected function processGeolocalisation(array &$element, array &$value){
    // //dcp('processGeolocalisation');
    // //dcp($element);
    // //dcp($value);

    if( isset($value['address']) && isset($value['postal_code']) && isset($value['city']) ) {
      $plugins = array(
        // 'openstreetmap' => 'openstreetmap',
        'googlemaps' => 'googlemaps',
      );
      $address = sprintf('%s, %s %s',
        $this->safeString($value['address']),
        $this->safeString($value['postal_code']),
        $this->safeString($value['city'])
      );
      $options = array(
        // 'openstreetmap' => array(), // array of options
        'googlemaps' => array(), // array of options
      );

      $geocode = \Drupal::service('geocoder')->geocode($address, $plugins, $options);

      if ($geocode) {
        $value['latitude'] = $geocode->first()->getCoordinates()->getLatitude();
        $value['longitude'] = $geocode->first()->getCoordinates()->getLongitude();
        $value['geo_status'] = LeafletMapElement::STATE_GEO_SUCCESS;
      } else {
        $value['geo_status'] = LeafletMapElement::STATE_GEO_FAILURE;
      }

      if ( $geocode && isset($element['#map_cluster']) && $element['#map_cluster']['type'] == 'polygon' ) {
        $this->loadShapes($element['#map_cluster']['file'],$element['#map_cluster']['key']);

        $point = $this->geophp()->load(json_encode(array(
          'type' => 'Point',
          'coordinates' => [$value['longitude'],$value['latitude']]
        )),'json');

        $map_cluster = false ;
        foreach ($this->shapes as $shape) {
          if( $shape['geophp']->contains($point) ) {
            $map_cluster = $shape;
            break;
          }
        }

        if ($map_cluster) {
          $value['cluster_id'] = $map_cluster['id'];
          // Full dump of properties into $data
          // foreach ($map_cluster['properties'] as $k => $v) {
          //   $value['cluster_info_'.$k] = $v;
          // }
        }
      }

    }
  }

  protected function format($type, array &$element, WebformSubmissionInterface $webform_submission, array $options = []) {
    //dcp('format');
    //dcp($type);
    //dcp($this->getItemsFormat($element));
    return parent::format($type, $element, $webform_submission, $options);
  }

  public function formatHtmlItem(array $element, WebformSubmissionInterface $webform_submission, array $options = []) {
    //dcp('formatHtml');
    //dcp($element['#type']);
    //dcp($element);
    //dcp($value);
    //dcp($options);
    //dcp($this->getItemsFormat($element));

    $value = $webform_submission->getData();
    $this->mergeDefaultValue($element, $value);

    // Return empty value.
    if (empty($value) || empty(array_filter($value))) {
      return '';
    }

    if (array_key_exists('email',$options) && $options['email']) {
      $format = 'text';
    } else {
      $format = $this->getItemsFormat($element);
    }

    // $format = 'list';
    //dcp($format);

    switch ($format) {
      case 'map':
        return $this->formatMap($element, $value, $options);
        break;
      case 'text':
        $lines = $this->formatLines($element, $value, $options);
        foreach ($lines as $key => $line) {
          $lines[$key] = [
            '#markup' => $line,
            '#suffix' => '<br/>'
          ];
        }
        return $lines;
        break;


      default:
        return parent::formatHtmlItem($element, $webform_submission, $options);
    }
  }

	/**
	* {@inheritdoc}
	*/
	protected function formatLines(array $element, array $value, array $options = []) {
    //dcp('formatLines');

    $lines = [];
		if (!(empty($value['address']) && empty($value['address']['address']))) {
			$lines['address'] = $this->safeString($value['address']['address']);
		}
		$location = '';
		if (!empty($value['city'])) {
			$location .= $this->safeString($value['city']);
		}
		if (!empty($value['postal_code'])) {
			$location .= ($location) ? '. ' : '';
			$location .= $this->safeString($value['postal_code']);
		}
		if ($location) {
			$lines['location'] = $location;
		}
		if (!empty($value['country'])) {
			$lines['country'] = $this->safeString($value['country']);
		}
		return $lines;
	}

	public function formatMap(array &$element, $value, array $options = []) {

    // $def = \Drupal::service('plugin.manager.geocoder.provider')->getDefinitions();

    if( empty($value['latitude']) || empty($value['longitude']) ) {
      $this->processGeolocalisation($element,$value);
    } else if ( isset($element['#map_cluster']) && $element['#map_cluster']['type'] == 'polygon' ) {
      $this->loadShapes($element['#map_cluster']['file'],$element['#map_cluster']['key']);
    }

    if ( isset($value['geo_status']) && $value['geo_status'] == LeafletMapElement::STATE_GEO_SUCCESS ) {

      $features[] = array(
        'type' => 'point',
        'lat' => $value['latitude'],
        'lon' => $value['longitude'],
        'icon' => array(
          'iconUrl'       => '/' . drupal_get_path('module', 'agora_map') . '/images/marker-icon-red.png',
          'iconSize'      => array('x' => '25', 'y' => '40'),
          'iconAnchor'    => array('x' => '12.5', 'y' => '40'),
          'popupAnchor'   => array('x' => '0', 'y' => '-32'),
          'shadowUrl'     => '/' . drupal_get_path('module', 'agora_map') . '/images/marker-shadow.png',
          'shadowSize'    => array('x' => '41', 'y' => '41'),
          'shadowAnchor'  => array('x' => '12.5', 'y' => '40'),
        ),
        //'popup' => l($node->title, 'node/' . $node->nid),
        'popup' => join('<br>', $this->formatLines($element, $value, $options)),
        'leaflet_id' => 'address_leaflet_map_points'
      );

      if ( $this->shapes && array_key_exists('cluster_id',$value) ) {
        $shape = $this->shapes[$value['cluster_id']];
        $features[] = array(
          'type' => 'json',
          'json' => $shape['geojson'], // JSON Object is needed
          'properties' => array(
            'style' => array(),
            'leaflet_id' => 'address_leaflet_map_json'
          )
        );
        $center = array('lat' => $shape['geophp']->getCentroid()->getX(), 'lng' => $shape['geophp']->getCentroid()->getY());
      } else {
        $center = array('lat' => $value['latitude'], 'lng' => $value['longitude']);
      }

  		$render = leaflet_render_map(array(
  			'label' => 'OSP Agora map',
  			'description' => t('Carte listant les lieux'),
  			'settings' => array(
  				'dragging' => TRUE,
  				'touchZoom' => TRUE,
  				'scrollWheelZoom' => TRUE,
  				'doubleClickZoom' => TRUE,
  				'zoomControl' => TRUE,
  				'attributionControl' => TRUE,
  				'trackResize' => TRUE,
  				'fadeAnimation' => TRUE,
  				'zoomAnimation' => TRUE,
  				'closePopupOnClick' => TRUE,
  				'center' => $center,
          'zoom' => 15
  			),
  			'layers' => array(
  				'earth' => array(
            // 'urlTemplate' => 'https://a.tiles.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
            // 'urlTemplate' => 'https://stamen-tiles-{s}.a.ssl.fastly.net/toner/{z}/{x}/{y}.png',
            'urlTemplate' => '//{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png',
            // 'urlTemplate' => 'http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png',
  					'options' => array(
  						'attribution' => 'OSP Agora map',
  						'maxZoom' =>	18,
              // 'id' => 'opensourcepolitics/ciwjaci0200362pqpii95xz65',
              // 'accessToken' => 'pk.eyJ1Ijoib3BlbnNvdXJjZXBvbGl0aWNzIiwiYSI6ImNpd2phYzI4NjAwMGEyb21zYjFkajZ0NTQifQ.c0s258Vrsz8RAyNzOaPx3g',
  					)
  				),
  			),
  		), $features , '600px');

      $render['#attached'] = NestedArray::mergeDeep(array(
        'library' => array(
          // 'agora_map/leaflet.markercluster',
          'agora_map/leaflet.fullscreen',
          'agora_map/leaflet.providers',
          // 'agora_map/leaflet.geoportal',
        )
      ), $render['#attached']);

    } else {

      // Geocoding Fail

      $render = new \Drupal\Core\StringTranslation\TranslatableMarkup('<div class="panel-body"><p>Désolé, nous n\'avons pas pu localiser votre adresse :<br/> <pre>@input</pre> <br/>Néanmoins, votre saisie sera bien prise en compte dans le décompte final.</p></div>', ['@input' => implode($this->formatLines($element, $value, $options),' ')]);
    }

    return $render;

	}

  /**
   * {@inheritdoc}
   */
  public function getItemDefaultFormat() {
    return 'map';
  }

  /**
   * {@inheritdoc}
   */
  public function getItemFormats() {
    return parent::getItemFormats() + [
      'text' => $this->t('Text block'),
      'json' => $this->t('JSON'),
      'map' => $this->t('Map'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsDefaultFormat() {
    return $this->getItemDefaultFormat();
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsFormats() {
    return $this->getItemFormats();
  }


    /**
     * {@inheritdoc}
     */
    public function buildExportOptionsForm(array &$form, FormStateInterface $form_state, array $export_options) {
      parent::buildExportOptionsForm($form, $form_state, $export_options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildExportHeader(array $element, array $options) {
      if (!empty($element['#multiple'])) {
        return parent::buildExportHeader($element, $options);
      }

      $composite_elements = $this->getInitializedCompositeElement($element);
      $header = [];
      foreach (RenderElement::children($composite_elements) as $composite_key) {
        $composite_element = $composite_elements[$composite_key];
        // if (isset($composite_element['#access']) && $composite_element['#access'] === FALSE) {
        //   continue;
        // }

        if ($options['header_format'] == 'label' && !empty($composite_element['#title'])) {
          $header[] = $composite_element['#title'];
        }
        else {
          $header[] = $composite_key;
        }
      }

      if(count($header) > 0) {
        $header[] = 'geo_status';
        $header[] = 'latitude';
        $header[] = 'longitude';
        $header[] = 'cluster_id';
      }

      return $this->prefixExportHeader($header, $element, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildExportRecord(array $element, WebformSubmissionInterface $webform_submission, array $export_options) {
      $value = $webform_submission->getData();
      if (!empty($element['#multiple'])) {
        $element['#format'] = ($export_options['header_format'] == 'label') ? 'list' : 'raw';
        $export_options['multiple_delimiter'] = PHP_EOL . '---' . PHP_EOL;
        return parent::buildExportRecord($element, $value, $export_options);
      }

      $record = [];
      $composite_elements = $this->getInitializedCompositeElement($element);
      foreach (RenderElement::children($composite_elements) as $composite_key) {
        $composite_element = $composite_elements[$composite_key];
        // if (isset($composite_element['#access']) && $composite_element['#access'] === FALSE) {
        //   continue;
        // }

        if ($export_options['composite_element_item_format'] == 'label' && $composite_element['#type'] != 'textfield' && !empty($composite_element['#options'])) {
          $record[] = WebformOptionsHelper::getOptionText($value[$composite_key], $composite_element['#options']);
        }
        else {
          $record[] = (isset($value[$composite_key])) ? $value[$composite_key] : NULL;
        }
      }

      if(count($record) > 0) {
        $record[] = $value['geo_status'];
        $record[] = $value['latitude'];
        $record[] = $value['longitude'];
        $record[] = $value['cluster_id'];
      }

      return $record;
    }

}
