/* global L */
(function ($) {
  'use strict';

  // override default Leaflet module create_feature to add new features
  Drupal.Leaflet.prototype.create_feature = function (feature) {
    var lFeature;
    switch (feature.type) {
      case 'point':
        lFeature = this.create_point(feature);
        // if a cluster has been created ...
        if (typeof lFeature !== 'undefined' && this.cluster) {

          if (feature.leaflet_id) {
            lFeature._leaflet_id = feature.leaflet_id;
            lFeature._leaflet_type = feature.type;
          }

          if (feature.popup) {
            lFeature.bindPopup(feature.popup, {maxWidth: '560px'});
          }

          // ... add the point to this cluster
          this.cluster.addLayer(lFeature);

        }
        // do not include the point on the map, it's already in the cluster
        return;
      case 'linestring':
        lFeature = this.create_linestring(feature);
        break;
      case 'polygon':
        lFeature = this.create_polygon(feature);
        break;
      case 'multipolygon':
      case 'multipolyline':
        lFeature = this.create_multipoly(feature);
        break;
      case 'json':
        lFeature = this.create_json(feature);
        break;
      case 'json-choropleth':
        lFeature = this.create_choropleth(feature);
        break;
      case 'markercluster':
        lFeature = this.create_markercluster(feature);
        lFeature._leaflet_type = feature.type;
        this.cluster = lFeature;
        break;
      case 'sidebar':
        lFeature = this.create_sidebar(feature);
        break;
      default:
        return; // Crash and burn.
    }

    // assign our given unique ID, useful for associating nodes
    if (feature.leaflet_id) {
      lFeature._leaflet_id = feature.leaflet_id;
      lFeature._leaflet_type = feature.type;
    }

    var options = {};
    if (feature.options) {
      for (var option in feature.options) {
        if (Object.prototype.hasOwnProperty.call(feature.options, option)) {
          options[option] = feature.options[option];
        }
      }
      lFeature.setStyle(options);
    }

    return lFeature;
  };

  // create a marker cluster
  Drupal.Leaflet.prototype.create_markercluster = function () {

    var CStyle = this.map_definition.layers.earth.options.CStyle;
    var animate = this.map_definition.layers.earth.options.animate;

    var markerCluster = new L.markerClusterGroup({
      maxClusterRadius: 30,
      singleMarkerMode: true,
      iconCreateFunction: function (cluster) {
        var c = cluster.getChildCount();
        var className = 'marker-cluster';
        className += ' marker-cluster-' + CStyle;
        if (animate) {
          // className += ' marker-cluster-animate';
        }
        var svg = '';

        // draw markers in SVG depending on the style
        if (CStyle === 'C19') {
          if (c === 1) {
            svg = '<path d="m 31.806238,54.297929 c 3.184222,3.620322 8.699256,3.971539 12.318665,0.7876 2.956869,-2.599995 3.731283,-6.757322 2.179788,-10.167251 l 0,0 c -1.3801,-3.031912 -1.153664,-6.553812 0.600551,-9.384191 l -8.79e-4,0 c 1.975281,-3.181742 1.738318,-7.406177 -0.864362,-10.36203 -3.183308,-3.620301 -8.698341,-3.973831 -12.316855,-0.789754 -2.957784,2.602141 -3.73218,6.75946 -2.179791,10.167265 l -8.78e-4,0 c 1.380101,3.034109 1.154558,6.553666 -0.601445,9.386316 l 0.0018,0 c -1.975281,3.181742 -1.738552,7.404024 0.862554,10.362045" />';
          }
          else if (c < 4) {
            className += ' marker-cluster-small';
            svg = '<path d="m 29.344399,59.658 c 4.348185,4.9437 11.879191,5.4233 16.821643,1.0755 4.037728,-3.5504 5.095223,-9.2274 2.976592,-13.8838 l 0,0 c -1.884584,-4.1402 -1.575376,-8.9495 0.820076,-12.8145 l -0.0012,0 c 2.697327,-4.3448 2.373745,-10.11344 -1.180322,-14.14978 -4.346938,-4.94367 -11.877944,-5.42643 -16.819174,-1.07844 -4.038975,3.55333 -5.096445,9.23032 -2.976593,13.88382 l -0.0012,0 c 1.884585,4.1432 1.576598,8.9493 -0.821298,12.8174 l 0.0024,0 c -2.697327,4.3448 -2.374064,10.1105 1.177853,14.1498" />';
          }
          else if (c < 12) {
            className += ' marker-cluster-medium';
            svg = '<path d="m 26.748238,64.007663 c 5.343678,6.075533 14.598865,6.664934 20.672864,1.32173 4.962143,-4.363244 6.261745,-11.339961 3.658066,-17.062418 l 0,0 C 48.763119,43.1789 49.143118,37.268537 52.086995,32.518667 l -0.0015,0 C 55.400386,27.17915 55.002721,20.089812 50.634971,15.129374 45.292826,9.053879 36.037638,8.4605938 29.965141,13.804032 c -4.963675,4.366845 -6.263247,11.34355 -3.658067,17.062443 l -0.0015,0 c 2.316051,5.091762 1.937552,10.998192 -1.009329,15.751871 l 0.0029,0 c -3.314865,5.339518 -2.917592,12.425242 1.447516,17.389317" />';
          }
          else {
            className += ' marker-cluster-large';
            svg = '<path d="m 24.924817,69.587132 c 6.600647,7.504653 18.032891,8.232697 25.53565,1.632634 6.129366,-5.38959 7.734667,-14.007409 4.518536,-21.075934 l 0,0 C 52.11816,43.858911 52.587545,36.55828 56.223897,30.691119 l -0.0019,0 C 60.316682,24.095611 59.825476,15.33868 54.43032,9.2114201 47.831566,1.7068135 36.399321,0.97397242 28.898418,7.5743239 22.767159,12.968362 21.161895,21.586167 24.37988,28.650289 l -0.0019,0 c 2.860845,6.289475 2.393314,13.585248 -1.246749,19.457114 l 0.0036,0 c -4.094606,6.595509 -3.603884,15.347977 1.788009,21.479729" />';
          }
        }
        else if (CStyle === 'C8') {
          if (c === 1) {
            svg = '<path d="M 40,30 l 0,20 M 30,40 l 20,0" class="cross"/>';
          }
          else if (c < 4) {
            className += ' marker-cluster-small';
            svg = '<path d="m 39.681083,27.079516 -1.719002,0 0,1.720962 -1.720073,0 0,1.717998 -1.719,0 0,1.717995 -1.720072,0 0,1.720726 -1.717859,0 0,1.717986 -1.719,0 0,1.720982 -1.720073,0 0,1.717991 -1.718977,0 0,13.755378 13.754056,0 0,-1.71799 1.72007,0 0,-1.720983 1.720073,0 0,-1.71799 1.719,0 0,-1.720706 1.720068,0 0,-1.720983 1.717883,0 0,-1.715037 1.718977,0 0,-1.720943 1.720073,0 0,-1.720746 1.719,0 0,-13.752634 -13.755144,0 0,1.717994 />"';
          }
          else if (c < 12) {
            className += ' marker-cluster-medium';
            svg = '<path d="m 40.867128,17.481954 -2.968766,0 0,2.972153 -2.970617,0 0,2.967033 -2.968764,0 0,2.967027 -2.970616,0 0,2.971745 -2.966792,0 0,2.967013 -2.968764,0 0,2.972187 -2.970617,0 0,2.96702 -2.968724,0 0,23.755944 23.75366,0 0,-2.96702 2.970612,0 0,-2.972187 2.970617,0 0,-2.96702 2.968764,0 0,-2.971711 2.970607,0 0,-2.972187 2.966835,0 0,-2.961921 2.968725,0 0,-2.972119 2.970616,0 0,-2.971779 2.968764,0 0,-23.751205 -23.75554,0 0,2.967027 />"';
          }
          else {
            className += ' marker-cluster-large';
            svg = '<path d="m 40.188236,9.0487286 -4.366868,0 0,4.37185 -4.36959,0 0,4.36432 -4.366865,0 0,4.36431 -4.36959,0 0,4.37125 -4.363965,0 0,4.36429 -4.366865,0 0,4.3719 -4.3695901,0 0,4.3643 -4.3668065,0 0,34.9435 34.9401396,0 0,-4.3643 4.369584,0 0,-4.3719 4.369591,0 0,-4.3643 4.366866,0 0,-4.3712 4.369576,0 0,-4.3719 4.364028,0 0,-4.3568 4.366807,0 0,-4.3718 4.36959,0 0,-4.3713 4.366865,0 0,-34.93653 -34.942907,0 0,4.36431 />"';
          }
        }
        else if (CStyle === 'C11') {
          if (c === 1) {
            svg = '<circle cx="40" cy="40" r="4" class="target"/>';
            svg += '<circle cx="40" cy="40" r="8" class="target"/>';
            svg += '<circle cx="40" cy="40" r="12" class="target"/>';
          }
          else if (c < 4) {
            className += ' marker-cluster-small';
            svg = '<circle cx="40" cy="40" r="15" class="big"/>';
          }
          else if (c < 12) {
            className += ' marker-cluster-medium';
            svg = '<circle cx="40" cy="40" r="20" class="big"/>';
          }
          else {
            className += ' marker-cluster-large';
            svg = '<circle cx="40" cy="40" r="25" class="big"/>';
          }
        }
        else if (CStyle === 'digits') {
          if (c === 1) {
            svg = '<path d="M45.6561214,18.3437507 C44.0936848,16.7811224 42.2082897,16 39.9999361,16 C37.7915824,16 35.9061873,16.7813142 34.3437507,18.3437507 C32.7813142,19.9061873 32,21.7915824 32,23.9999361 C32,25.135469 32.1718354,26.0677145 32.5155063,26.7968644 L38.2186778,38.890741 C38.374979,39.2344118 38.6171979,39.505206 38.9453345,39.7029318 C39.2734711,39.9008494 39.625005,40 40.0001279,40 C40.3750589,40 40.7267846,39.9008494 41.0549212,39.7029318 C41.3830578,39.505206 41.630263,39.2344118 41.7969203,38.890741 L47.4841741,26.7968644 C47.8280367,26.0677145 47.9998721,25.135469 47.9998721,23.9999361 C48.0000639,21.7915824 47.2187498,19.9063791 45.6561214,18.3437507 L45.6561214,18.3437507 Z M42.8279328,26.8281246 C42.0468104,27.609247 41.1042088,27.9999041 39.9999361,27.9999041 C38.8956634,27.9999041 37.9530617,27.609247 37.1717475,26.8281246 C36.3904333,26.0468104 35.9997763,25.1042088 35.9997763,23.9999361 C35.9997763,22.8958551 36.3904333,21.9530617 37.1717475,21.1719393 C37.9530617,20.3906251 38.8958551,19.999968 39.9999361,19.999968 C41.1042088,19.999968 42.0468104,20.3906251 42.8279328,21.1719393 C43.609247,21.9530617 43.9999041,22.8958551 43.9999041,23.9999361 C43.9999041,25.104017 43.609247,26.0466187 42.8279328,26.8281246 L42.8279328,26.8281246 Z"></path>';
          }
          else if (c < 4) {
            className += ' marker-cluster-small';
            svg += '<circle cx="40" cy="40" r="15" />';
          }
          else if (c < 12) {
            className += ' marker-cluster-medium';
            svg += '<circle cx="40" cy="40" r="20" />';
          }
          else {
            className += ' marker-cluster-large';
            svg += '<circle cx="40" cy="40" r="25" />';
          }
          if (c > 1) {
            svg += '<text x="50%" y="50%" dy="0.35em" text-anchor="middle">' + c + '</text>';
          }
        }

        return L.divIcon({
          html: '<svg viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">' + svg + '</svg>',
          className: className,
          iconSize: L.point(60, 60)
        });
      }
    });
    this.lMap.markerCluster = markerCluster;
    return markerCluster;
  };

  // Drupal.Leaflet.prototype.create_geoportal_layer = function (feature) {
  //   return new L.tileLayer.wms(feature.urlTemplate, feature.options);
  // };

  Drupal.Leaflet.prototype.create_json = function (feature) {
    // console.log('create_json');

    var gjs = new L.GeoJSON(feature.json);
    // console.dir(gjs);

    gjs.setStyle(feature.properties.style);
    return gjs;

    // return new L.GeoJSON(feature.json, {
    //   style: function (feature) {
    //       if (feature.properties.style) {
    //         return feature.properties.style ;
    //       }
    //       return {};
    //   }
    //   onEachFeature: function (feature, layer) {
    //     layer.bindPopup(feature.properties.popup);
    //     for (var layer_id in layer._layers) {
    //       for (var i in layer._layers[layer_id]._latlngs) {
    //         Drupal.Leaflet.bounds.push(layer._layers[layer_id]._latlngs[i]);
    //       }
    //     }
    //
    //     if (feature.properties.style) {
    //       layer.setStyle(feature.properties.style);
    //     }
    //
    //     if (feature.properties.leaflet_id) {
    //       layer._leaflet_id = feature.properties.leaflet_id;
    //     }
    //   }
    // });
  };

  Drupal.Leaflet.prototype.create_choropleth = function (feature) {
    var choro = new L.choropleth(feature.json, {
      // valueProperty: 'count', // which property in the features to use
      valueProperty: function (feature) {
        if (feature.debug) {
          var min = Math.ceil(0);
          var max = Math.floor(100);
          var count = Math.floor(Math.random() * (max - min + 1)) + min;
          return count;
        }
        else {
          return feature.properties.count;
        }

      }, // which property in the features to use
      scale: ['white', 'yellow', 'red'], // chroma.js scale - include as many as you like
      steps: 5, // number of breaks or steps in range
      mode: 'e', // q for quantile, e for equidistant, k for k-means
      style: feature.properties.style,
      onEachFeature: function (feature, layer) {
        layer.bindPopup('<b>' + feature.properties.nom_iris + '</b>');
        layer.setStyle(feature.properties.style);
      }
    });

    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function (map) {
      var div = L.DomUtil.create('div', 'info choropleth-legend');
      var limits = choro.options.limits;
      var colors = choro.options.colors;
      var labels = [];
      // Add min & max
      div.innerHTML = '<div class="labels"><div class="min">' + limits[0] + '</div> \
        <div class="middle">' + Math.floor(limits[Math.floor(limits.length / 2)]) + '</div></div> \
        <div class="max">' + limits[limits.length - 1] + '</div></div>';

      limits.forEach(function (limit, index) {
        labels.push('<li style="background-color: ' + colors[index] + '"></li>');
      });

      div.innerHTML += '<ul>' + labels.join('') + '</ul>';
      return div;
    };
    legend.addTo(this.lMap);

    return choro;
  };

  Drupal.Leaflet.prototype.create_sidebar = function (feature) {
    console.log('Agora Leaflet View : creating sidebar'); // eslint-disable-line no-console
    L.control.sidebar('leaflet-sidebar-left').addTo(this.lMap);
  };

})(jQuery);
