<?php

/**
 * @file
 * Contains \Drupal\agora_mail\Controller\AgoraMailController.
 */

namespace Drupal\agora_mail\Controller;

use Drupal\Core\Controller\ControllerBase;

class AgoraMailController extends ControllerBase {
  public function content() {

    return array(
      '#theme' => 'agora_mail',
      '#test_var' => $this->t('Test Value'),
    );

  }
}
