<?php

/**
 * @file
 * Module file for Examples for Developers.
 *
 * This file serves as a stub file for the many Examples modules in the
 * @link http://drupal.org/project/examples Examples for Developers Project @endlink
 * which you can download and experiment with.
 *
 * One might say that examples.module is an example of documentation. However,
 * note that the example submodules define many doxygen groups, which may or
 * may not be a good strategy for other modules.
 */

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * @defgroup agora agora
 * @{
 * Well-documented API examples for a broad range of Drupal core functionality.
 *
 * Developers can learn how to use a particular API quickly by experimenting
 * with the examples, and adapt them for their own use.
 *
 * Download the Examples for Developers Project (and participate with
 * submissions, bug reports, patches, and documentation) at
 * http://drupal.org/project/examples
 */

/**
 * Implements hook_toolbar().
 */
function agora_toolbar() {
  // First, build an array of all example modules and their routes.
  // We resort to this hard-coded way so as not to muck up each example.
  $agora = array(
    'agora_home' => 'agora.home',
    'agora_page' => 'entity.agora_page.collection',
    'agora_agenda' => 'agora_agenda',
    'agora_mail' => 'agora_mail.description',
    'agora_settings' => 'agora_settings',
    // 'agora_editor' => 'agora_editor',
  );

  // Build a list of links for the menu.
  $links = array();
  foreach ($agora as $module => $route) {
    // Get the module info (title, description) from Drupal.
    $info = system_get_info('module', $module);

    // If there's no info, the example isn't enabled, so don't display it.
    if (!empty($info)) {
      $links[$module] = array(
        'title' => t($info['name']),
        'url' => Url::fromRoute($route),
        'attributes' => array(
          'title' => t($info['description']),
        ),
      );
    }
  }

  // Add a link to enable all modules.
  $links['enable_agora'] = array(
    'title' => t('Enable agora'),
    'url' => Url::fromRoute('system.modules_list'),
    'options' => array(
      'title' => t('Enable more modules in on the Extend page.'),
    ),
    'fragment' => 'edit-modules-agora-modules',
  );

  // Create the agora toolbar render array.
  $items['agora'] = array(
    '#type' => 'toolbar_item',
    'tab' => array(
      '#type' => 'link',
      '#title' => t('agora'),
      // @todo: Once it is created, use the example index page.
      '#url' => Url::fromRoute('<front>'),
      '#attributes' => array(
        'title' => t('Developer agora'),
        'class' => array('toolbar-icon', 'toolbar-icon-agora'),
      ),
    ),
    'tray' => array(
      '#heading' => t('Agora Developer'),
      'shortcuts' => array(
        '#theme' => 'links__toolbar_example',
        '#links' => $links,
        '#attributes' => array(
          'class' => array('toolbar-menu'),
        ),
      ),
    ),
    '#weight' => 99,
    '#attached' => array(
      'library' => array(
        'agora/agora.icons',
      ),
    ),
  );

  return $items;
}

/**
 * @} End of 'defgroup agora'.
 */
