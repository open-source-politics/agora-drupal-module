<?php

namespace Drupal\agora_editor\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class HomeController extends ControllerBase {

  /**
   * Constructs a simple page.
   *
   * The router _controller callback, maps the path
   * 'agora/agora_editor/page' to this method.
   *
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function page() {
    return array(
      '#markup' => $this->t('Simple page: The quick brown fox jumps over the lazy dog.'),
    );
  }

}
