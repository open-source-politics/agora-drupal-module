<?php

/**
 * @file
 * Definition of \Drupal\agora_editor\Plugin\CKEditorPlugin\AnchorLink.
 */
namespace Drupal\agora_editor\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;

/**
 * Defines the "basewidget" plugin.
 *
 * @CKEditorPlugin(
 *   id = "basewidget",
 *   label = @Translation("CKEditor Layout Manager"),
 *   module = "agora_editor"
 * )
 */
class BaseWidget extends CKEditorPluginBase implements CKEditorPluginCssInterface {

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
     */
    function getFile() {
        return drupal_get_path('module', 'agora_editor') . '/js/plugins/basewidget/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return array();
    }
    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function isInternal() {
        return FALSE;
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
     */
    function getButtons() {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getCssFiles(Editor $editor) {
      return array();
    }
}
