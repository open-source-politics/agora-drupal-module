<?php

/**
 * @file
 * Definition of \Drupal\agora_editor\Plugin\CKEditorPlugin\AnchorLink.
 */
namespace Drupal\agora_editor\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;

/**
 * Defines the "layoutmanager" plugin.
 *
 * @CKEditorPlugin(
 *   id = "layoutmanager",
 *   label = @Translation("CKEditor Layout Manager"),
 *   module = "agora_editor"
 * )
 */
class LayoutManager extends CKEditorPluginBase {

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
     */
    function getFile() {
        return drupal_get_path('module', 'agora_editor') . '/js/plugins/layoutmanager/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return array('basewidget');
    }
    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        return array('core/jquery');
    }

    /**
     * {@inheritdoc}
     */
    public function isInternal() {
        return FALSE;
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
     */
    function getButtons() {
        return array(
            'AddLayout' => array(
                'label' => $this->t('Add Layout'),
                'image' => drupal_get_path('module', 'agora_editor') . '/js/plugins/layoutmanager/icons/addlayout.png',
            ),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        return array();
    }
}
