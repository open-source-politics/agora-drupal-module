<?php

/**
 * @file
 * Definition of \Drupal\agora_editor\Plugin\CKEditorPlugin\AnchorLink.
 */
namespace Drupal\agora_editor\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginCssInterface;
use Drupal\ckeditor\CKEditorPluginContextualInterface;

/**
 * Defines the "basewidget" plugin.
 *
 * @CKEditorPlugin(
 *   id = "config_extra",
 *   label = @Translation("CKEditor Config extras"),
 *   module = "agora_editor"
 * )
 */
class ConfigExtra extends CKEditorPluginBase implements CKEditorPluginCssInterface, CKEditorPluginContextualInterface {

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
     */
    function getFile() {
        return drupal_get_path('module', 'agora_editor') . '/js/plugins/config_extra/plugins.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return array();
    }
    /**
     * {@inheritdoc}
     */
    public function getLibraries(Editor $editor) {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function isInternal() {
        return FALSE;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled(Editor $editor) {
      return TRUE;
    }

    /**
     * Implements \Drupal\ckeditor\Plugin\CKEditorPluginButtonsInterface::getButtons().
     */
    function getButtons() {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getCssFiles(Editor $editor) {
      return array(
        drupal_get_path('theme', 'sage') . '/dist/styles/main.css'
      );
    }
}
