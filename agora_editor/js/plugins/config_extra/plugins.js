(function ($) {
  'use strict';

  CKEDITOR.plugins.add('config_extra', {
    icons: '',
    init: pluginInit
  });

  function pluginInit(editor) {
    CKEDITOR.dtd.$removeEmpty.i = 0;
    CKEDITOR.dtd.$removeEmpty.span = 0;
    CKEDITOR.dtd.$removeEmpty.div = 0;
  }

})(jQuery);
