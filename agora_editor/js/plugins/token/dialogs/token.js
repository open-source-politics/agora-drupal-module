/**
 * Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 */

// Note: This automatic widget to dialog window binding (the fact that every field is set up from the widget
// and is committed to the widget) is only possible when the dialog is opened by the Widgets System
// (i.e. the widgetDef.dialog property is set).
// When you are opening the dialog window by yourself, you need to take care of this by yourself too.
(function ($) {
  'use strict';
  CKEDITOR.dialog.add('token', function (editor) {
    return {
      title: 'Edit Token',
      minWidth: 200,
      minHeight: 100,
      contents: [{
        id: 'info',
        elements: [{
          id: 'token',
          type: 'text',
          label: 'token format without [ ]',
          width: '100%',
          setup: function (widget) {
            this.setValue(widget.data.token);
          },
          commit: function (widget) {
            widget.setData('token', this.getValue());
          }
        }]
      }]
    };
  });
})(jQuery);
