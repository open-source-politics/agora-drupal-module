/**
 * Copyright (c) 2014-2016, CKSource - Frederico Knabben. All rights reserved.
 * Licensed under the terms of the MIT License (see LICENSE.md).
 *
 * Simple CKEditor Widget (Part 2).
 *
 * Created out of the CKEditor Widget SDK:
 * http://docs.ckeditor.com/#!/guide/widget_sdk_tutorial_2
 */

(function ($) {
  'use strict';

  // Register the plugin within the editor.
  CKEDITOR.plugins.add('token', {
    // This plugin requires the Widgets System defined in the 'widget' plugin.
    requires: 'widget',

    // Register the icon used for the toolbar button. It must be the same
    // as the name of the widget.
    icons: 'token',

    // The plugin initialization logic goes inside this method.
    init: function (editor) {
      // Register the editing dialog.
      CKEDITOR.dialog.add('token', this.path + 'dialogs/token.js');

      // Register the token widget.
      editor.widgets.add('token', {
        // Allow all HTML elements, classes, and styles that this widget requires.
        // Read more about the Advanced Content Filter here:
        // * http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
        // * http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
        allowedContent: 'p a div span h2 h3 h4 h5 h6 section article iframe object embed strong b i em cite pre blockquote small sub sup code ul ol li dl dt dd table thead tbody th tr td img caption mediawrapper br[href,src,target,token,height,colspan,span,alt,name,title,class,id,data-options]{text-align,float,margin}(*)',

        // Define the template of a new Simple Box widget.
        // The template will be used when creating new instances of the Simple Box widget.
        template: '<span data-token="" class="token"></span>',

        // Define the label for a widget toolbar button which will be automatically
        // created by the Widgets System. This button will insert a new widget instance
        // created from the template defined above, or will edit selected widget
        // (see second part of this tutorial to learn about editing widgets).
        //
        // Note: In order to be able to translate your widget you should use the
        // editor.lang.token.* property. A string was used directly here to simplify this tutorial.
        button: 'Create token',

        // Set the widget dialog window name. This enables the automatic widget-dialog binding.
        // This dialog window will be opened when creating a new widget or editing an existing one.
        dialog: 'token',

        // Check the elements that need to be converted to widgets.
        //
        // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
        // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
        // during data processing which is done on DOM represented by JavaScript objects.
        upcast: function (element) {
          // Return "true" (that element needs to converted to a Simple Box widget)
          // for all <div> elements with a "token" class.
          return element.name === 'span' && element.hasClass('token');
        },

        // When a widget is being initialized, we need to read the data ("align" and "token")
        // from DOM and set it by using the widget.setData() method.
        // More code which needs to be executed when DOM is available may go here.
        init: function () {
          var token = this.element.getAttribute('data-token');
          if (token) {
            this.setData('token', token);
          }
        },

        // Listen on the widget#data event which is fired every time the widget data changes
        // and updates the widget's view.
        // Data may be changed by using the widget.setData() method, which we use in the
        // Simple Box dialog window.
        data: function () {
          this.element.setAttribute('data-token', this.data.token);
          this.element.setHtml('[' + this.data.token + ']');
        }
      });
    }
  });
})(jQuery);
