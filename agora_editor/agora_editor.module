<?php

/**
 * @file
 * Module file for agora_editor_module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * @defgroup agora_editor Example: Page
 * @ingroup examples
 * @{
 * This example demonstrates how a module can display a page at a given URL.
 *
 * It's important to understand how the menu system works in order to
 * implement your own pages. See the Menu Example module for some insight.
 *
 * @see menu_example
 */

/**
 * Implements hook_help().
 *
 * Through hook_help(), a module can make documentation available to the user
 * for the module as a whole or for specific routes. Where the help appears
 * depends on the $route_name specified.
 *
 * Help text will be displayed in the region designated for help text. Typically
 * this is the 'Help' region which can be found at admin/structure/block.
 *
 * The help text in the first example below, will appear on the simple page at
 * agora/agora_editor/page.
 *
 * The second example text will be available on the admin help page (admin/help)
 * in the list of help topics using the name of the module. To specify help in
 * the admin section combine the special route name of 'help.page' with the
 * module's machine name, as in 'help.page.agora_editor' below.
 *
 * See the Help text standard page for the proposed format of help texts.
 *
 * @see https://www.drupal.org/documentation/help-text-standards
 *
 * @see hook_help()
 */
function agora_editor_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    case 'agora_editor':
      // Help text for the simple page registered for this path.
      return t('This is help text for the simple page.');

    case 'help.page.agora_editor':
      // Help text for the admin section, using the module name in the path.
      return t("This is help text created in page example's implementation of hook_help().");
  }
}

/**
 * Implements hook_libraries_info().
 */
function agora_editor_libraries_info() {
  $libraries['ckeditor.codemirror'] = array(
    'name' => 'CKEditor CodeMirror plugin',
    'vendor url' => 'https://w8tcha.github.io/CKEditor-CodeMirror-Plugin',
    'download url' => 'https://github.com/w8tcha/CKEditor-CodeMirror-Plugin/releases',
    'version arguments' => array(
      'file' => 'codemirror/plugin.js',
      'pattern' => '@version:\s+([0-9a-zA-Z\.-]+)@',
    ),
    'files' => array(
      'js' => array(
        'codemirror/plugin.js',
        'codemirror/js/codemirror.min.js',
      ),
      'css' => array(
        'codemirror/css/codemirror.min.css',
      ),
    ),
    'variants' => array(
      'minified' => array(
        'files' => array(
          'js' => array(
            'codemirror/js/codemirror.min.js',
          ),
          'css' => array(
            'codemirror/css/codemirror.min.css',
          ),
        ),
      ),
      'source' => array(
        'files' => array(
          'js' => array(
            'codemirror/js/codemirror.js',
          ),
          'css' => array(
            'codemirror/css/codemirror.css',
            'codemirror/css/codemirror.ckeditor.css',
          ),
        ),
      ),
    ),
  );

  return $libraries;
}


function agora_editor_editor_js_settings_alter(array &$settings) {
  foreach (array_keys($settings['editor']['formats']) as $text_format_id) {
    if ($settings['editor']['formats'][$text_format_id]['editor'] === 'ckeditor') {
      if ($library_path = libraries_get_path('ckeditor.minimalist')) {
        $library_url = '/'. $library_path . '/';
        $settings['editor']['formats'][$text_format_id]['editorSettings']['skin'] = 'Minimalist,'. $library_url ;
      }
    }
  }
}


/**
 * @} End of "defgroup agora_editor".
 */
