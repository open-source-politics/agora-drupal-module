<?php

namespace Drupal\agora_page;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Page entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup agora_page
 */
interface PageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
