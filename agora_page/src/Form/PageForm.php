<?php

namespace Drupal\agora_page\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;

/**
 * Form controller for the agora_page entity edit forms.
 *
 * @ingroup agora_page
 */
class PageForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\agora_page\Entity\Page */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['name']['#title_display'] = 'invisible';

    //dcp($form);

    $form['header'] = [
      '#type' => 'details',
      '#title' => $this->t('Header'),
      '#weight' => 1,
    ];

    $form['header']['display_title'] = $form['display_title'] ;
    $form['header']['display_title']['#weight'] = 2 ;
    $form['display_title'] = null;

    $form['header']['description'] = $form['description'];
    $form['header']['description']['#weight'] = 3 ;
    $form['description'] = null;

    $form['header']['header_image'] = $form['header_image'] ;
    $form['header']['header_image']['#weight'] = 4 ;
    $form['header_image'] = null;


    // @see \Drupal\field_group\Plugin\field_group\FieldGroupFormatter\HtmlElement
    $form['page_options'] = array(
      '#type' => 'field_group_html_element',
      '#weight' => 90,
      '#wrapper_element' => 'ul',
      '#group_name' => 'page_options',
      '#entity_type' => 'page',
      '#bundle' => 'agora',
      '#attributes' => new Attribute( array('class' => 'list-group form-options') ),
      'item_0' => array(
        '#type' => 'field_group_html_element',
        '#weight' => 10,
        '#wrapper_element' => 'li',
        '#group_name' => 'item_0',
        '#entity_type' => 'page',
        '#bundle' => 'agora',
        '#attributes' => new Attribute( array('class' => 'list-group-item form-inline') ),
      ),
      'item_1' => array(
        '#type' => 'field_group_html_element',
        '#weight' => 20,
        '#wrapper_element' => 'li',
        '#group_name' => 'item_0',
        '#entity_type' => 'page',
        '#bundle' => 'agora',
        '#attributes' => new Attribute( array('class' => 'list-group-item form-inline') ),
      ),
    );

    if( $form['path'] ){
      $form['path']['#display_classes'] = 'list-group-item inline';
      $form['page_options']['item_0']['path'] = $form['path'];
      $form['path'] = null;
    }

    $form['page_options']['item_1']['langcode'] = array(
      '#title' => $this->t('Language'),
      '#type' => 'language_select',
      '#default_value' => $entity->getUntranslated()->language()->getId(),
      '#languages' => Language::STATE_ALL,
      '#display_classes' => 'list-group-item inline',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $form_state->setRedirect('entity.agora_page.collection');
    $entity = $this->getEntity();
    $entity->save();
  }

}
