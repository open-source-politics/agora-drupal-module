<?php

namespace Drupal\agora_page\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\agora_page\PageInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the ContentEntityExample entity.
 *
 * @ingroup agora_page
 *
 * This is the main definition of the entity type. From it, an entityType is
 * derived. The most important properties in this example are listed below.
 *
 * id: The unique identifier of this entityType. It follows the pattern
 * 'moduleName_xyz' to avoid naming conflicts.
 *
 * label: Human readable name of the entity type.
 *
 * handlers: Handler classes are used for different tasks. You can use
 * standard handlers provided by D8 or build your own, most probably derived
 * from the standard class. In detail:
 *
 * - view_builder: we use the standard controller to view an instance. It is
 *   called when a route lists an '_entity_view' default for the entityType
 *   (see routing.yml for details. The view can be manipulated by using the
 *   standard drupal tools in the settings.
 *
 * - list_builder: We derive our own list builder class from the
 *   entityListBuilder to control the presentation.
 *   If there is a view available for this entity from the views module, it
 *   overrides the list builder. @todo: any view? naming convention?
 *
 * - form: We derive our own forms to add functionality like additional fields,
 *   redirects etc. These forms are called when the routing list an
 *   '_entity_form' default for the entityType. Depending on the suffix
 *   (.add/.edit/.delete) in the route, the correct form is called.
 *
 * - access: Our own accessController where we determine access rights based on
 *   permissions.
 *
 * More properties:
 *
 *  - base_table: Define the name of the table used to store the data. Make sure
 *    it is unique. The schema is automatically determined from the
 *    BaseFieldDefinitions below. The table is automatically created during
 *    installation.
 *
 *  - fieldable: Can additional fields be added to the entity via the GUI?
 *    Analog to content types.
 *
 *  - entity_keys: How to access the fields. Analog to 'nid' or 'uid'.
 *
 *  - links: Provide links to do standard tasks. The 'edit-form' and
 *    'delete-form' links are added to the list built by the
 *    entityListController. They will show up as action buttons in an additional
 *    column.
 *
 * There are many more properties to be used in an entity type definition. For
 * a complete overview, please refer to the '\Drupal\Core\Entity\EntityType'
 * class definition.
 *
 * The following construct is the actual definition of the entity type which
 * is read and cached. Don't forget to clear cache after changes.
 *
 * @ContentEntityType(
 *   id = "agora_page",
 *   label = @Translation("Page entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\agora_page\Entity\Controller\PageListBuilder",
 *     "form" = {
 *       "add" = "Drupal\agora_page\Form\PageForm",
 *       "edit" = "Drupal\agora_page\Form\PageForm",
 *       "delete" = "Drupal\agora_page\Form\PageDeleteForm",
 *     },
 *     "access" = "Drupal\agora_page\PageAccessControlHandler",
 *   },
 *   list_cache_contexts = { "user" },
 *   base_table = "page",
 *   admin_permission = "administer agora_page entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/agora/page/{agora_page}",
 *     "edit-form" = "/agora/page/{agora_page}/edit",
 *     "delete-form" = "/agora/page/{agora_page}/delete",
 *     "collection" = "/agora/page/list"
 *   },
 *   field_ui_base_route = "agora_page.settings",
 * )
 *
 * The 'links' above are defined by their path. For core to find the
 * corresponding route, the route name must follow the correct pattern:
 *
 * entity.<entity-name>.<link-name> (replace dashes with underscores)
 * Example: 'entity.agora_page.canonical'
 *
 * See routing file above for the corresponding implementation
 *
 * The Page class defines methods and fields for the page entity.
 *
 * Being derived from the ContentEntityBase class, we can override the methods
 * we want. In our case we want to provide access to the standard fields about
 * creation and changed time stamps.
 *
 * Our interface (see PageInterface) also exposes the EntityOwnerInterface.
 * This allows us to provide methods for setting and providing ownership
 * information.
 *
 * The most important part is the definitions of the field properties for this
 * entity type. These are of the same type as fields added through the GUI, but
 * they can by changed in code. In the definition we can define if the user with
 * the rights privileges can influence the presentation (view, edit) of each
 * field.
 *
 * The class also uses the EntityChangedTrait trait which allows it to record
 * timestamps of save operations.
 */
class Page extends ContentEntityBase implements PageInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += array(
      'user_id' => \Drupal::currentUser()->id(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Page entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Page entity.'))
      ->setReadOnly(TRUE);

    // Name field for the page.
    // We set display options for the view as well as the form.
    // Users with correct privileges can change the view and edit configuration.
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the page.'))
      ->setTranslatable(TRUE)
      ->setSettings(array(
        'default_value' => '',
        'max_length' => 255,
        'text_processing' => 0,
        'title_display' => 'invisible',
      ))
      ->setDisplayOptions('view', array(
        'label' => 'visually_hidden',
        'type' => 'string',
        'weight' => -5,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'string_textfield',
        'weight' => -6,
        'settings' => array(
          'placeholder' => t('Page Name'),
        )
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

      $fields['display_title'] = BaseFieldDefinition::create('boolean')
        ->setLabel(t('Display Title'))
        ->setDescription(t('Display title in the page header'))
        ->setTranslatable(TRUE)
        ->setDisplayOptions('view', array(
          'label' => 'visually_hidden',
          'type' => 'hidden',
          'weight' => -4,
        ))
        ->setDisplayOptions('form', array(
          'type' => 'boolean_checkbox',
          'settings' => array(
            'display_label' => TRUE,
          ),
          'weight' => -4,
        ))
        ->setDisplayConfigurable('form', TRUE);

      $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDescription(t('A small text to be displayed in the page header.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'visually_hidden',
        'type' => 'text_default',
        'weight' => -3,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea',
        'weight' => -3,
      ));

      $fields['header_image'] = BaseFieldDefinition::create('image')
        ->setLabel(t('Image intro'))
        ->setDescription(t('The uploaded image will be displayed on this page using the image style choosen below.'))
        ->setDisplayOptions('view', array(
          'label' => 'visually_hidden',
          'type' => 'hidden',
          'weight' => -2,
        ))
        ->setDisplayOptions('form', array(
          'type' => 'image_image',
          'weight' => -2,
        ));

      $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Page body'))
      ->setDescription(t('The page main content'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', array(
        'label' => 'visually_hidden',
        'type' => 'text_default',
        'weight' => 1,
      ))
      ->setDisplayOptions('form', array(
        'type' => 'text_textarea',
        'weight' => 10,
      ))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Owner field of the page.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // $fields['path'] = BaseFieldDefinition::create('path')
    //   ->setLabel(t('Path'))
    //   ->setDescription(t('the path'))
    //   ->setCustomStorage(TRUE)
    //   ->setTranslatable(TRUE)
    //   ->setComputed(TRUE)
    //   ->setDisplayOptions('form', array(
    //     'type' => 'path',
    //   ));

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of ContentEntityExample entity.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
