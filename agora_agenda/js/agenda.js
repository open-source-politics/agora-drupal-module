/**
 * @file
 * Contains the definition of the behaviour agenda.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  $(function () {
    console.log('JQUERY agenda.js');
    console.log(Drupal);
    console.log(drupalSettings);

    $('.cibulSearch').append('<button role="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>');

  });

})(jQuery, Drupal, drupalSettings);
