/**
 * @file
 * Contains the definition of the behaviour agenda.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';
  $(function () {
    $(window).load(function () {
      // console.log('window load');
      // console.dir(drupalSettings);

      jQuery('.icon-panel .state-refresh').fadeIn(250);

      var controller = window.cibul.getController($('.agenda-preview .cbpgmp.cibulMap').attr('data-cbctl'));
      // controller.disableSyncHref();

      if (!controller) {
        jQuery('.icon-panel .state-refresh').fadeOut(250);
        jQuery('.icon-panel .state-empty').fadeIn(250);
      }
      else {
        // console.log('controller %0', controller.getWidget('map'));

        $('.agenda-preview .cbpgmp.cibulMap').click(function () {
          $(this).find('.map-canvas').css('pointer-events', 'auto');
        }).mouseleave(function () {
          $(this).find('.map-canvas').css('pointer-events', 'none');
        });

        var list = [];

        controller.register({
          name: 'site',
          include: function (t, e) {
            list.push(t.u);
          },
          change: function () {
            jQuery('.icon-panel .state-empty').fadeOut(250);
            jQuery('.sidebar .list-panel').fadeOut(250, function () {
              jQuery('.sidebar .list-panel ul').empty();
            });
            jQuery('.sidebar .item-panel').fadeOut(250, function () {
              jQuery('.sidebar .item-panel .item').remove();
            });
            jQuery('.icon-panel .state-refresh').fadeIn(250);
            list.length = 0;
          },
          enable: function (newSearchValues) {

            if (list.length === 0) {
              jQuery('.icon-panel .state-refresh').fadeOut(250, function () {
                jQuery('.icon-panel .state-empty').fadeIn(250);
              });
            }
            else {

              for (var i = 0; i < Math.min(list.length, 4); i++) {

                jQuery.ajax({
                  url: 'https://openagenda.com/agendas/' + drupalSettings.agora.agenda.oaid + '/events.json',
                  data: {
                    'oaq[uids][]': list[i]
                  },
                  cached: true,
                  dataType: 'jsonp',
                  crossDomain: true
                }).done(function (res) {
                  console.log('response %O', res);
                  if (res.events && res.events.length > 0) {
                    var e = res.events[0];

                    var listPanel = jQuery('.sidebar .list-panel');
                    var itemPanel = jQuery('.sidebar .item-panel');

                    var li = jQuery('<button role="button" class="list-group-item"></button>');
                    listPanel.find('ul').append(li);

                    var itemWrapper = jQuery('<div class="item panel panel-default"></div>');
                    itemWrapper.attr('data-uid', e.uid);

                    var item = jQuery('<div class="panel-footer"></div>');

                    itemWrapper.append(item).appendTo(itemPanel);

                    var time;
                    var location;
                    var dateList;

                    if (e.locations) {
                      // for (var i = 0; i < e.locations[0].dates.length; i++) {
                      for (var i = 0; i < 1; i++) {
                        var date = new Date(e.locations[0].dates[i].date);
                        var cal = jQuery('<div class="date media"></div>');

                        jQuery('<div class="media-left media-middle">')
                          .append('<div class="day media-object">' + date.getDate() + '</div>')
                          .appendTo(cal);

                        time = jQuery('<div class="time"></div>');

                        jQuery('<i class="fa fa-clock-o"></i>')
                          .appendTo(time);

                        var start = e.locations[0].dates[i].timeStart.split(':');
                        var timeValue = start[0] + 'h' + start[1];

                        if (e.locations[0].dates[i].timeEnd) {
                          var end = e.locations[0].dates[i].timeEnd.split(':');
                          timeValue += ' - ' + end[0] + 'h' + end[1];
                        }

                        time.append(timeValue);

                        jQuery('<div class="media-body"></div>')
                          .append('<span class="month">' + date.toLocaleDateString('fr-FR', {
                            month: 'long'
                          }) + '</span>')
                          .append('<span class="year">' + date.getFullYear() + '</span>')
                          .append(time)
                          .appendTo(cal);

                        cal.appendTo(item);

                        dateList = jQuery('<div class="media-object media-top date"></div>')
                          .append('<div class="day">' + date.toLocaleDateString('fr-FR', {
                            day: '2-digit'
                          }) + '</span>')
                          .append('<span class="month">' + date.toLocaleDateString('fr-FR', {
                            month: 'short'
                          }) + '</span>');

                        location = jQuery('<div class="location media"></div>');

                        jQuery('<div class="media-left media-middle"></div>')
                          .append('<i class="fa fa-map-marker media-object"></i>')
                          .appendTo(location);

                        jQuery('<div class="media-body"></div>')
                          .append('<span>' + e.locations[0].placename + '</span>')
                          .append('<br/>')
                          .append('<span>' + e.locations[0].address + '</span>')
                          .appendTo(location);
                      }
                    }



                    jQuery('<h2></h2>').html(e.title.fr).appendTo(item);
                    jQuery('<p></p>').html(e.description.fr).appendTo(item);

                    item.append(location);

                    jQuery('<div class="media"></div>')
                      .append(jQuery('<div class="media-left"></div>').append(dateList))
                      .append(jQuery('<div class="media-body"></div>')
                        .append(jQuery('<h5></h5>').html(e.title.fr))
                      ).appendTo(li);


                    jQuery('<a class="links btn btn-primary btn-more pull-right"></a>')
                      .attr('href', e.link)
                      .text('En savoir plus')
                      .appendTo(item);

                    if (list.length > 1) {

                      jQuery('<a class="links btn btn-link btn-back pull-left"></a>')
                        .append('<i class="fa fa-list"></i>')
                        .append('&nbsp;Retour liste')
                        .click(function () {
                          itemWrapper.removeClass('active');
                          itemPanel.fadeOut(250);
                          listPanel.fadeIn(250);
                        })
                        .appendTo(item);

                    }

                    // List Events

                    li.click(function () {
                      itemWrapper.addClass('active');
                      listPanel.fadeOut(250);
                      itemPanel.fadeIn(250);
                    });


                    jQuery('.icon-panel .state-refresh').fadeOut(250);

                    if (list.length === 1) {
                      itemWrapper.addClass('active');
                      itemPanel.fadeIn(250);
                    }
                    else {
                      listPanel.fadeIn(250);
                    }
                  }
                });
              }

            }

          }

        });
      }

    });

  });

})(jQuery, Drupal, drupalSettings);
