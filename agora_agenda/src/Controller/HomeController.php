<?php

namespace Drupal\agora_agenda\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class HomeController extends ControllerBase {

  /**
   * Constructs a page with descriptive content.
   *
   * Our router maps this method to the path 'agora/agora_agenda'.
   */
  public function description() {
    // Make our links. First the simple page.
    $agora_agenda_simple_link = Link::createFromRoute($this->t('page agenda'), 'agora_agenda_page')->toString();

    // Assemble the markup.
    $build = array(
      '#markup' => $this->t('<div class="container well"><p>This content can be edited in <code>modules/contrib/agora/agora_agenda/src/Controller/AgendaController.php</code>.</p><p>The @simple_link just returns a renderable array for display.</p></div>',
        array(
          '@simple_link' => $agora_agenda_simple_link,
        )
      ),
    );

    return $build;
  }

  /**
   * Constructs a simple page.
   *
   * The router _controller callback, maps the path
   * 'agora/agora_agenda/page' to this method.
   *
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function page() {

    $config = $this->config('agora.settings');

    return array(
      '#theme' => 'agora_agenda',
      '#test_var' => $this->t('Simple page: The quick brown fox jumps over the lazy dog.'),
      '#agenda' => array(
        'oalink' => $config->get('oalink'),
        'oaid' => $config->get('oaid'),
        'oatoken' => $config->get('oatoken'),
        'enable_filter' => TRUE,
      ),
      '#attached' => array(
        'library' =>  array(
          'agora_agenda/agenda.main',
          'agora_agenda/agenda.oalib',
        ),
      ),
    );
  }

}
