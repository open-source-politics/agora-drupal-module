<?php

namespace Drupal\agora_agenda\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Example: configurable text string' block.
 *
 * Drupal\Core\Block\BlockBase gives us a very useful set of basic functionality
 * for this configurable block. We can just fill in a few of the blanks with
 * defaultConfiguration(), blockForm(), blockSubmit(), and build().
 *
 * @Block(
 *   id = "agora_agenda_preview",
 *   admin_label = @Translation("Agenda Preview")
 * )
 */
class AgendaPreviewBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return array(
      'agora_agenda_category' => '',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['agora_agenda_category_text'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Block contents'),
      '#description' => $this->t('This text will appear in the example block.'),
      '#default_value' => $this->configuration['agora_agenda_category'],
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['agora_agenda_category']
      = $form_state->getValue('agora_agenda_category_text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = \Drupal::config('agora.settings');

    // //dcp('build');
    // //dcp($this->configuration);

    $agenda = array(
      'oalink' => $config->get('oalink'),
      'oaid' => $config->get('oaid'),
      'oatoken' => $config->get('oatoken'),
      'oacat' => $this->configuration['agora_agenda_category'],
      'enable_filter' => FALSE,
    );

    return array(
      '#theme' => 'agora_agenda_preview',
      '#agenda' => $agenda,
      '#attached' => array(
        'library' =>  array(
          'agora_agenda/agenda.preview',
        ),
        'drupalSettings' => array(
          'agora' => array(
            'settings' => $config->get(),
            'agenda' => $agenda,
          ),
        ),
      ),
    );
  }

}
