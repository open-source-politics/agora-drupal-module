<?php

namespace Drupal\agora_home\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Component\Utility\Random;
use Drupal\Component\Render\FormattableMarkup;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for page example routes.
 */
class HomeController extends ControllerBase {

  protected function paragraphs($count=1) {
    $random = new Random();
    $paragraphs = array();
    for ($i=0; $i < $count; $i++) {
      $paragraphs[] = sprintf('<p>%s</p>',$random->paragraphs(1));
    }
    return new FormattableMarkup(implode('',$paragraphs),array());
  }

  /**
   * Constructs a simple page.
   *
   * The router _controller callback, maps the path
   * 'agora/agora_home/page' to this method.
   *
   * _controller callbacks return a renderable array for the content area of the
   * page. The theme system will later render and surround the content with the
   * appropriate blocks, navigation, and styling.
   */
  public function page() {
    drupal_set_message($this->paragraphs(1),'status');
    drupal_set_message($this->paragraphs(1),'status');
    drupal_set_message($this->paragraphs(4),'info');
    drupal_set_message($this->paragraphs(1),'warning');
    drupal_set_message($this->paragraphs(1),'error');

    return array(
      '#theme' => 'agora_home',
      '#test_var' => $this->t('Simple page: The quick brown fox jumps over the lazy dog.'),
    );
  }

}
