<?php

namespace Drupal\Tests\agora_settings\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Creates page and render the content based on the arguments passed in the URL.
 *
 * @group agora_settings
 * @group examples
 */
class PageExampleTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('agora_settings');

  /**
   * The installation profile to use with this test.
   *
   * We need the 'minimal' profile in order to make sure the Tool block is
   * available.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * User object for our test.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $webUser;

  /**
   * Generates a random string of ASCII numeric characters (values 48 to 57).
   *
   * @param int $length
   *   Length of random string to generate.
   *
   * @return string
   *   Randomly generated string.
   */
  protected static function randomNumber($length = 8) {
    $str = '';
    for ($i = 0; $i < $length; $i++) {
      $str .= chr(mt_rand(48, 57));
    }
    return $str;
  }

  /**
   * Verify that current user has no access to page.
   *
   * @param string $url
   *   URL to verify.
   */
  public function pageExampleVerifyNoAccess($url) {
    // Test that page returns 403 Access Denied.
    $this->drupalGet($url);
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Data provider for testing menu links.
   *
   * @return array
   *
   *   Array of page -> link relationships to check for, with the permissions
   *   required to access them:
   *   - Permission machine name. Empty string means no login.
   *   - Array of link information:
   *     - Key is path to the page where the link should appear.
   *     - Value is the link that should appear on the page.
   */
  public function providerMenuLinks() {
    return [
      [
        '',
        ['' => '/agora/agora_settings'],
      ],
      [
        'access simple page',
        ['/agora/agora_settings' => '/agora/settings'],
      ],
    ];
  }

  /**
   * Verify and validate that default menu links were loaded for this module.
   *
   * @dataProvider providerMenuLinks
   */
  public function testPageExampleLinks($permission, $links) {
    if ($permission) {
      $user = $this->drupalCreateUser(array($permission));
      $this->drupalLogin($user);
    }
    foreach ($links as $page => $path) {
      $this->drupalGet($page);
      $this->assertSession()->linkByHrefExists($path);
    }
    if ($permission) {
      $this->drupalLogout();
    }
  }

  /**
   * Main test.
   *
   * Login user, create an example node, and test page functionality through
   * the admin and user interfaces.
   */
  public function testPageExample() {
    $assert_session = $this->assertSession();
    // Verify that anonymous user can't access the pages created by
    // agora_settings module.
    $this->pageExampleVerifyNoAccess('agora/settings');

    // Create a regular user and login.
    $this->webUser = $this->drupalCreateUser();
    $this->drupalLogin($this->webUser);

    // Verify that regular user can't access the pages created by
    // agora_settings module.
    $this->pageExampleVerifyNoAccess('agora/settings');

    // Create a user with permissions to access 'simple' page and login.
    $this->webUser = $this->drupalCreateUser(array('access simple page'));
    $this->drupalLogin($this->webUser);

    // Verify that user can access simple content.
    $this->drupalGet('/agora/settings');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains('The quick brown fox jumps over the lazy dog.');

    // Check if user can't access simple page.
    $this->pageExampleVerifyNoAccess('agora/settings');
  }

}
