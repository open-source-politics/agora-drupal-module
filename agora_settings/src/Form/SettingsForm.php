<?php

namespace Drupal\agora_settings\Form;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\file\Entity\File;


/**
 * Implements the SimpleForm form controller.
 *
 * This example demonstrates a simple form with a singe text input element. We
 * extend FormBase which is the simplest form base class used in Drupal.
 *
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class SettingsForm extends ConfigFormBase {

  /**
  * Getter method for Form ID.
  *
  * The form ID is used in implementations of hook_form_alter() to allow other
  * modules to alter the render array built by this form controller.  it must
  * be unique site wide. It normally starts with the providing module's name.
  *
  * @return string
  *   The unique ID of the form defined by this class.
  */
  public function getFormId() {
    return 'agora_settings_form';
  }

  /**
  * {@inheritdoc}
  */
  protected function getEditableConfigNames() {
    return [
      'system.site',
      'agora.settings',
    ];
  }

  /**
   * Build the settings form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $system = $this->config('system.site');
    $config = $this->config('agora.settings');

    $form['general'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('General'),
    ];

    $form['general']['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      // '#description' => $this->t('Title must be at least 5 characters in length.'),
      '#required' => TRUE,
      '#default_value' => $system->get('name'),
    ];

    $form['general']['slogan'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Slogan'),
      '#required' => FALSE,
      '#default_value' => $system->get('slogan'),
    ];

    $form['general']['hero_image'] = array(
      '#title' => t('Image intro'),
      '#type' => 'managed_file',
      '#description' => t('The uploaded image will be displayed on this page using the image style choosen below.'),
      '#default_value' => $config->get('image_hero_fid'),
      '#upload_location' => 'public://upload/',
      '#cardinality' => 1,
    );

    $form['agenda'] = [
      '#type' => 'fieldgroup',
      '#title' => $this->t('Agenda'),
    ];

    $form['agenda']['oalink'] = [
      '#type' => 'url',
      '#title' => $this->t('Open Agenda link'),
      '#required' => FALSE,
      '#default_value' => $config->get('oalink'),
    ];

    $form['agenda']['oaid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Open Agenda ID'),
      '#required' => FALSE,
      '#maxlength' => 8,
      '#default_value' => $config->get('oaid'),
    ];

    $form['agenda']['oatoken'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Open Agenda token'),
      '#required' => FALSE,
      '#maxlength' => 8,
      '#default_value' => $config->get('oatoken'),
    ];


    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (strlen($values['title']) < 5) {
      // Set an error for the form element with a key of "title".
      $form_state->setErrorByName('title', $this->t('The title must be at least 5 characters long.'));
    }

    if ( !empty($values['oaid']) && !is_numeric($values['oaid']) ) {
      $form_state->setErrorByName('oaid', $this->t('Wrong format: only numbers.'));
    }

    if ( !empty($values['oatoken']) && !is_numeric($values['oatoken']) ) {
      $form_state->setErrorByName('oatoken', $this->t('Wrong format: only numbers.'));
    }

    return parent::validateForm($form, $form_state);
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /*
     * This would normally be replaced by code that actually does something
     * with the title.
     */
    $values = $form_state->getValues();
    $system = \Drupal::service('config.factory')->getEditable('system.site');
    $system->set('name', $values['title'])->save();
    $system->set('slogan', $values['slogan'])->save();

    $config = \Drupal::service('config.factory')->getEditable('agora.settings');

    $fid = 14;
    $file = $fid ? File::load($fid) : FALSE;
    if ($file) {
      // When a module is managing a file, it must manage the usage count.
      // Here we decrement the usage count with file_usage_delete().
      \Drupal::service('file.usage')->delete($file, 'agora_settings', 'hero_image');

      // The file_delete() function takes a file object and checks to see if
      // the file is being used by any other modules. If it is the delete
      // operation is cancelled, otherwise the file is deleted.
      $file->delete();


      drupal_set_message(t('The image @image_name with ID @image_fid was removed.', array(
        '@image_name' => $file->getFilename(),
        '@image_fid' => $file->id(),
      )));

    }

    if ($values['hero_image'][0] != $config->get('image_hero_fid')[0]) {

      // Retrieve the old file's id.
      $fid = $config->get('image_hero_fid')[0];
      $file = $fid ? File::load($fid) : FALSE;
      if ($file) {
        // When a module is managing a file, it must manage the usage count.
        // Here we decrement the usage count with file_usage_delete().
        \Drupal::service('file.usage')->delete($file, 'agora_settings', 'hero_image');

        // The file_delete() function takes a file object and checks to see if
        // the file is being used by any other modules. If it is the delete
        // operation is cancelled, otherwise the file is deleted.
        $file->delete();


        drupal_set_message(t('The image @image_name with ID @image_fid was removed.', array(
          '@image_name' => $file->getFilename(),
          '@image_fid' => $file->id(),
        )));

      }

      // If fid is not 0 we have a valid file.
      if ( $values['hero_image'][0] != 0 ) {
        // The new file's status is set to 0 or temporary and in order to ensure
        // that the file is not removed after 6 hours we need to change it's status
        // to 1. Save the ID of the uploaded image for later use.
        $file = File::load($values['hero_image'][0]);
        $file->status = FILE_STATUS_PERMANENT;
        $file->save();

        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
        $file->setOwner($user);

        \Drupal::service('file.usage')->add($file, 'agora_settings', 'hero_image', $this->getFormId());

        $config->set('image_hero_fid', $values['hero_image'])->save();
        $config->set('image_hero_path', $file->getFileUri())->save();

        drupal_set_message(t('The image @image_name was uploaded and saved with an ID of @fid and will be displayed using the style @style.',
        array(
          '@image_name' => $file->getFilename(),
          '@fid' => $file->id(),
          '@style' => $values['thumbnail'],
        )
        ));
      }
      // If file was removed, we remove the configuration
      else {
        $config->set('image_hero_fid', NULL)->save();
        $config->set('image_hero_path', NULL)->save();
      }

    }


    $config->set('oalink', $values['oalink'])->save();
    $config->set('oaid', $values['oaid'])->save();
    $config->set('oatoken', $values['oatoken'])->save();

    parent::submitForm($form, $form_state);

  }

}
