<?php

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info().
 */
function agora_settings_token_info() {

  $types['agora'] = [
    'name' => t('Agora'),
    'description' => t('Agora tokens')
  ];

  $agora['hero'] = [
    'name' => t('Hero image'),
    'description' => t('The main hero image'),
  ];

  $types['icon'] = [
    'name' => t('Icon template'),
    'description' => t('Icons tokens')
  ];

  $icon['fa'] = [
    'name' => t('Font Awesome'),
    'description' => t('Font Awesome icons'),
  ];

  return [
    'types' => $types,
    'tokens' => [
      'icon' => $icon,
      'agora' => $agora,
    ],
  ];

}

/**
 * Implements hook_tokens().
 */
function agora_settings_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {

  $token_service = \Drupal::token();

  $url_options = array('absolute' => TRUE);
  if (isset($options['langcode'])) {
    $url_options['language'] = \Drupal::languageManager()->getLanguage($options['langcode']);
    $langcode = $options['langcode'];
  }
  else {
    $langcode = NULL;
  }
  $replacements = array();

  if ($type == 'agora') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'hero':
          $config = \Drupal::config('agora.settings');
          $bubbleable_metadata->addCacheableDependency($config);
          $replacements[$original] = $config->get('image_hero');
          break;
      }
    }
  }

  elseif ($type == 'icon') {
    foreach ($tokens as $name => $original) {
      if (strpos($name, 'fa:') === 0) {
        // Parse [icon:fa:{class}:{class}:{class}...] token with optional format.
        $classes = explode(':', $name);
        if( count($classes) >= 2 ) {
          $replacements[$original] = Markup::create(sprintf('<i class="%s"></i>',implode(' fa-',$classes)));
        }
      }
    }
  }

  return $replacements;
}
